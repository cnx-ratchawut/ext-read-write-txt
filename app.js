var app = angular.module('ngApp', []);

app.controller('uploadController', function ($scope, fileReader) {
  console.log(fileReader)

  $scope.getFile = function () {
    $scope.progress = 0;
    $scope.textSrc = '';
    fileReader.readAsText($scope.file, $scope)
      .then(function (result) {
        var lines = result.split('\n');
        $scope.textSrc =
          `$$FILEM SET  PAD=' '
$$FILEM FCH  INPUT=DDIN,
$$FILEM      NOUPDATE=YES,MEMBER=*,LIST=SUMMARY,
$$FILEM      PROC=*`;
        for (var i = 0; i < lines.length; i++) {
          var res = lines[i].toUpperCase()
          res = res.replace(/[^A-Z0-9-]/g, '');
          if (i === 0) {
            $scope.textSrc += `
IF (FLD_CO(1,0,C,'${res}'))`;
          }
          else {
            $scope.textSrc += `
|   (FLD_CO(1,0,C,'${res}'))`
          }
        }
        $scope.textSrc += `
THEN DO
  WRITE(MEMLIST)
END
ELSE DO
  WRITE(NONLIST)
END
/+`
      });
  };

  $scope.$on("fileProgress", function (e, progress) {
    $scope.progress = progress.loaded / progress.total;
  });
  $scope.onClickClearHandler = () => { 
    $scope.textSrc = null;
    // $('#ngUpload').value = '';
  }
  $scope.onClickHandler = () => {
    if (!$scope.textSrc) {
      alert('value is null')
      return;
    }
    var filename = 'DWSSCAN.CARDIN.txt';
    var blob = new Blob([$scope.textSrc], { type: 'text/plain' }),
      e = document.createEvent('MouseEvents'),
      a = document.createElement('a')
    // FOR IE:

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
    else {
      var e = document.createEvent('MouseEvents'),
        a = document.createElement('a');

      a.download = filename;
      a.href = window.URL.createObjectURL(blob);
      a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
      e.initEvent('click', true, false, window,
        0, 0, 0, 0, 0, false, false, false, false, 0, null);
      a.dispatchEvent(e);
    }
  }
});


app.directive("ngFileSelect", function () {
  return {
    link: function ($scope, el) {
      el.bind("change", function (e) {
        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile();
      })
    }
  }
});